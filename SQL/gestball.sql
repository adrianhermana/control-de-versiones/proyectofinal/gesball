-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 12-06-2020 a las 22:03:36
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `gestball`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clubs`
--

CREATE TABLE `clubs` (
  `id` int(11) NOT NULL,
  `nombre` varchar(55) NOT NULL,
  `telefono` varchar(13) NOT NULL,
  `email` varchar(85) NOT NULL,
  `correo` varchar(10) NOT NULL,
  `nombre_responsable` varchar(55) NOT NULL,
  `apellido_responsable` varchar(55) NOT NULL,
  `telefono_responsable` varchar(13) NOT NULL,
  `licencia_club` char(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `clubs`
--

INSERT INTO `clubs` (`id`, `nombre`, `telefono`, `email`, `correo`, `nombre_responsable`, `apellido_responsable`, `telefono_responsable`, `licencia_club`) VALUES
(1, 'A.D. Pumas', '666999666', 'pumas@hotmail.com', '39600', 'Adrian', 'Hermana', '666111666', '5FJB'),
(2, 'Cantbasket', '999888999', 'cantbasket@gmail.com', '39700', 'Mercedes', 'Fernandez', '999111999', 'JDLK');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `entrenadores`
--

CREATE TABLE `entrenadores` (
  `id` int(11) NOT NULL,
  `nombre` varchar(55) NOT NULL,
  `apellido` varchar(55) NOT NULL,
  `dni` char(9) NOT NULL,
  `licencia` char(1) NOT NULL,
  `cod_licencia` char(4) NOT NULL,
  `email` varchar(85) NOT NULL,
  `telefono` varchar(13) NOT NULL,
  `temporada` char(9) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `entrenadores`
--

INSERT INTO `entrenadores` (`id`, `nombre`, `apellido`, `dni`, `licencia`, `cod_licencia`, `email`, `telefono`, `temporada`) VALUES
(1, 'Francisco', 'Antonio', '72569984E', 'A', 'FUEL', 'franciscoantonio@hotmail.com', '856974321', '2020-2021'),
(2, 'Adrian', 'Hermana', '75621365E', 'S', 'FREW', 'adrianhf97basket@hotmail.com', '987423654', '2020-2021'),
(3, 'Joaquin', 'Ruiz', '87563154R', 'A', 'ASSA', 'joaquin@hotmail.com', '965324587', '2020-2021');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `equipos`
--

CREATE TABLE `equipos` (
  `id` int(11) NOT NULL,
  `id_entrenadores` int(11) NOT NULL,
  `id_clubs` int(11) NOT NULL,
  `id_ligas` int(11) NOT NULL,
  `nombre` varchar(55) NOT NULL,
  `licencia_equipo` char(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `equipos`
--

INSERT INTO `equipos` (`id`, `id_entrenadores`, `id_clubs`, `id_ligas`, `nombre`, `licencia_equipo`) VALUES
(1, 2, 1, 1, 'A.D. Pumas ', 'ERTF'),
(2, 2, 1, 2, 'Pumas B', 'JKTR'),
(3, 1, 2, 1, 'Cantbasket A', 'HTRE'),
(4, 1, 2, 2, 'Cantbasket B', 'FTRD'),
(5, 3, 2, 1, 'Cantbasket Cinemark A', 'FTRE'),
(6, 3, 2, 2, 'Cantbasket Cinemark B', 'REDS');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `jugadores`
--

CREATE TABLE `jugadores` (
  `id` int(11) NOT NULL,
  `id_equipos` int(11) NOT NULL,
  `dni` char(9) NOT NULL,
  `nombre` varchar(55) NOT NULL,
  `apellidos` varchar(55) NOT NULL,
  `cod_licencia` char(4) NOT NULL,
  `fecha_licencia` date NOT NULL,
  `caducidad_licencia` date NOT NULL,
  `temporada` char(9) NOT NULL,
  `email` varchar(85) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `jugadores`
--

INSERT INTO `jugadores` (`id`, `id_equipos`, `dni`, `nombre`, `apellidos`, `cod_licencia`, `fecha_licencia`, `caducidad_licencia`, `temporada`, `email`) VALUES
(1, 1, '56845624W', 'Luka', 'Doncic', 'FGRE', '2020-05-03', '2022-05-06', '2020-2021', 'luka@gmail.com'),
(2, 1, '87654321W', 'James', 'Harden', '56FE', '2020-05-03', '2026-05-19', '2020-2021', 'james@gmail.com'),
(3, 1, '98745563W', 'Kawhi', 'Leonard', 'JFYE', '2020-05-04', '2024-05-19', '2020-2021', 'kawhi@gmail.com'),
(4, 1, '99988554F', 'Lebron', 'James', 'UR45', '2020-05-01', '2023-08-28', '2020-2021', 'lebron@gmail.com\r\n'),
(5, 1, '99988554F', 'Anthony', 'Davis', 'UR46', '2020-05-01', '2023-04-28', '2020-2021', 'davis@gmail.com\r\n'),
(6, 1, '98564635F', 'Damian', 'Lillard', 'UHFD', '2020-05-01', '2023-08-28', '2020-2021', 'lillard@gmail.com\r\n'),
(7, 1, '98564515F', 'Chris', 'Paul', '89FD', '2020-05-01', '2023-08-28', '2020-2021', 'paul@gmail.com\r\n'),
(8, 1, '98564335F', 'Russell', 'Westbrook', 'U89D', '2020-05-01', '2023-08-28', '2020-2021', 'westbrook@gmail.com\r\n'),
(9, 1, '98589215F', 'Donovan', 'Mitchell', '8AXV', '2020-05-01', '2023-08-28', '2020-2021', 'donovan@gmail.com\r\n'),
(10, 1, '98511335F', 'Brandon', 'Ingram', 'UOIU', '2020-05-01', '2023-08-28', '2020-2021', 'brandon@gmail.com\r\n'),
(11, 2, '32145624W', 'Trae', 'Young', 'F5RE', '2020-05-03', '2022-05-06', '2020-2021', 'trae@gmail.com'),
(12, 2, '32154321W', 'Kemba', 'Walker', '55FE', '2020-05-03', '2026-05-19', '2020-2021', 'kemba@gmail.com'),
(13, 2, '32145563W', 'Giannis', 'Antetokounmpo', 'J5YE', '2020-05-04', '2024-05-19', '2020-2021', 'giannis@gmail.com'),
(14, 2, '32188554F', 'Pascal', 'Siakam', 'U545', '2020-05-01', '2023-08-28', '2020-2021', 'pascal@gmail.com\r\n'),
(15, 2, '32188554F', 'Joel', 'Embid', 'U546', '2020-05-01', '2023-04-28', '2020-2021', 'joel@gmail.com\r\n'),
(16, 2, '32164635F', 'Kyle', 'Lowry', 'U5FD', '2020-05-01', '2023-08-28', '2020-2021', 'kyle@gmail.com\r\n'),
(17, 2, '32164515F', 'Ben', 'Simmons', '85FD', '2020-05-01', '2023-08-28', '2020-2021', 'ben@gmail.com\r\n'),
(18, 2, '32164335F', 'Jimmy', 'Butler', 'U59D', '2020-05-01', '2023-08-28', '2020-2021', 'jimmy@gmail.com\r\n'),
(19, 2, '32189215F', 'Jayson', 'Tatum', '85XV', '2020-05-01', '2023-08-28', '2020-2021', 'jayson@gmail.com\r\n'),
(20, 2, '3211335F', 'Khris', 'Middleton', 'U5IU', '2020-05-01', '2023-08-28', '2020-2021', 'khris@gmail.com\r\n'),
(21, 3, '42145624W', 'Juan Carlos', 'Navarro', 'F6RE', '2020-05-03', '2022-05-06', '2020-2021', 'juan@gmail.com'),
(22, 3, '42154321W', 'Felipe', 'Reyes', '56FE', '2020-05-03', '2026-05-19', '2020-2021', 'felipe@gmail.com'),
(23, 3, '42145563W', 'Rudy', 'Fernandez', 'J6YE', '2020-05-04', '2024-05-19', '2020-2021', 'rudy@gmail.com'),
(24, 3, '42188554F', 'Pau', 'Gasol', 'U645', '2020-05-01', '2023-08-28', '2020-2021', 'pau@gmail.com\r\n'),
(25, 3, '42188554F', 'Marc', 'Gasol', 'U646', '2020-05-01', '2023-04-28', '2020-2021', 'marc@gmail.com\r\n'),
(26, 3, '42164635F', 'Jose Manuel', 'Calderon', 'U6FD', '2020-05-01', '2023-08-28', '2020-2021', 'jossemanuel@gmail.com\r\n'),
(27, 3, '42164515F', 'Jorge', 'Garbajosa', '86FD', '2020-05-01', '2023-08-28', '2020-2021', 'jorge@gmail.com\r\n'),
(28, 3, '42164335F', 'Victor', 'Claver', 'U69D', '2020-05-01', '2023-08-28', '2020-2021', 'victor@gmail.com\r\n'),
(29, 3, '42189215F', 'Sergio', 'Rodriguez', '86XV', '2020-05-01', '2023-08-28', '2020-2021', 'sergio@gmail.com\r\n'),
(30, 3, '43211335F', 'Ricard', 'Rubio', 'U6IU', '2020-05-01', '2023-08-28', '2020-2021', 'ricard@gmail.com\r\n'),
(31, 4, '62145624W', 'Lamar', 'Odon', 'F7RE', '2020-05-03', '2022-05-06', '2020-2021', 'lamar@gmail.com'),
(32, 4, '62154321W', 'Michael', 'Jordan', '57FE', '2020-05-03', '2026-05-19', '2020-2021', 'micahel@gmail.com'),
(33, 4, '62145563W', 'Kobe', 'Bryant', 'J7YE', '2020-05-04', '2024-05-19', '2020-2021', 'kobe@gmail.com'),
(34, 4, '62188554F', 'Dzanan', 'Musa', 'U745', '2020-05-01', '2023-08-28', '2020-2021', 'musa@gmail.com\r\n'),
(35, 4, '62188554F', 'Kevin', 'Durant', 'U746', '2020-05-01', '2023-04-28', '2020-2021', 'kevin@gmail.com\r\n'),
(36, 4, '62164635F', 'Garrett', 'Temple', 'U7FD', '2020-05-01', '2023-08-28', '2020-2021', 'garrett@gmail.com\r\n'),
(37, 4, '62164515F', 'Andre', 'Drummond', '87FD', '2020-05-01', '2023-08-28', '2020-2021', 'andre@gmail.com\r\n'),
(38, 4, '62164335F', 'Dwight', 'Howard', 'U79D', '2020-05-01', '2023-08-28', '2020-2021', 'dwight@gmail.com\r\n'),
(39, 4, '66189215F', 'Kevin', 'Love', '87XV', '2020-05-01', '2023-08-28', '2020-2021', 'kevin@gmail.com\r\n'),
(40, 4, '63211335F', 'Hassan', 'Whiteside', 'U7IU', '2020-05-01', '2023-08-28', '2020-2021', 'hassan@gmail.com\r\n'),
(41, 5, '72145624W', 'Andre', 'Iguodala', 'F8RE', '2020-05-03', '2022-05-06', '2020-2021', 'andre@gmail.com'),
(42, 5, '72154321W', 'Udonis', 'Haslem', '58FE', '2020-05-03', '2026-05-19', '2020-2021', 'udonis@gmail.com'),
(43, 5, '72145563W', 'Tacko', 'Fall', 'J8YE', '2020-05-04', '2024-05-19', '2020-2021', 'tacko@gmail.com'),
(44, 5, '72188554F', 'Kristaps', 'Porzingis', 'U845', '2020-05-01', '2023-08-28', '2020-2021', 'kristaps@gmail.com\r\n'),
(45, 5, '72188554F', 'Boban', 'Marjanovic', 'U846', '2020-05-01', '2023-04-28', '2020-2021', 'bodan@gmail.com\r\n'),
(46, 5, '72164635F', 'Thabo', 'Sefolosha', 'U8FD', '2020-05-01', '2023-08-28', '2020-2021', 'thabo@gmail.com\r\n'),
(47, 5, '72164515F', 'Vince', 'Carter', '88FD', '2020-05-01', '2023-08-28', '2020-2021', 'vince@gmail.com\r\n'),
(48, 5, '72164335F', 'Kyle', 'Korver', 'U89D', '2020-05-01', '2023-08-28', '2020-2021', 'kyle@gmail.com\r\n'),
(49, 5, '76189215F', 'Chris', 'Clemons', '88XV', '2020-05-01', '2023-08-28', '2020-2021', 'chris@gmail.com\r\n'),
(50, 5, '73211335F', 'Tyson', 'Chandler', 'U8IU', '2020-05-01', '2023-08-28', '2020-2021', 'tyson@gmail.com\r\n'),
(51, 6, '82145624W', 'Sekou', 'Doumbouya', 'F9RE', '2020-05-03', '2022-05-06', '2020-2021', 'sekou@gmail.com'),
(52, 6, '82154321W', 'Alen', 'Smailagic', '59FE', '2020-05-03', '2026-05-19', '2020-2021', 'alen@gmail.com'),
(53, 6, '82145563W', 'Zion', 'Williamson', 'J9YE', '2020-05-04', '2024-05-19', '2020-2021', 'zion@gmail.com'),
(54, 6, '82188554F', 'Jalen', 'Lecque', 'U945', '2020-05-01', '2023-08-28', '2020-2021', 'jalen@gmail.com\r\n'),
(55, 6, '82188554F', 'Rj', 'Barrett', 'U946', '2020-05-01', '2023-04-28', '2020-2021', 'rj@gmail.com\r\n'),
(56, 6, '82164635F', 'Klemen', 'Prepelic', 'U9FD', '2020-05-01', '2023-08-28', '2020-2021', 'klemen@gmail.com\r\n'),
(57, 6, '82164515F', 'Nikola', 'Mirotic', '89FD', '2020-05-01', '2023-08-28', '2020-2021', 'nikola@gmail.com\r\n'),
(58, 6, '82164335F', 'Giorgi', 'Shermadini', 'U99D', '2020-05-01', '2023-08-28', '2020-2021', 'giorgi@gmail.com\r\n'),
(59, 6, '86189215F', 'Jordi', 'Trias', '89XV', '2020-05-01', '2023-08-28', '2020-2021', 'jordi@gmail.com\r\n'),
(60, 6, '83211335F', 'Axel', 'Bouteille', 'U9IU', '2020-05-01', '2023-08-28', '2020-2021', 'axel@gmail.com\r\n');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ligas`
--

CREATE TABLE `ligas` (
  `id` int(11) NOT NULL,
  `nombre` varchar(55) NOT NULL,
  `cod_liga` char(4) NOT NULL,
  `division` varchar(2) NOT NULL,
  `temporada` char(9) NOT NULL,
  `fecha_inicio` date NOT NULL,
  `fecha_final` date NOT NULL,
  `num_equipos` varchar(2) NOT NULL,
  `comunidad` varchar(20) NOT NULL,
  `provincia` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ligas`
--

INSERT INTO `ligas` (`id`, `nombre`, `cod_liga`, `division`, `temporada`, `fecha_inicio`, `fecha_final`, `num_equipos`, `comunidad`, `provincia`) VALUES
(1, 'LIGA ACB', 'FGRT', '1A', '2020-2021', '2020-05-11', '2021-05-20', '3', 'Cantabria', 'Santander'),
(2, 'LEB ORO', 'UTJD', '2A', '2020-2021', '2020-05-10', '2021-04-29', '3', 'Cantabria', 'Santander');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `partidos`
--

CREATE TABLE `partidos` (
  `id` int(11) NOT NULL,
  `id_liga` int(11) NOT NULL,
  `id_local` int(11) NOT NULL,
  `id_visitante` int(11) NOT NULL,
  `Marcador_local` varchar(3) NOT NULL,
  `Marcador_visitante` varchar(3) NOT NULL,
  `ventaja_maxima_local` varchar(3) NOT NULL,
  `ventaja_mmaxima_visitante` varchar(3) NOT NULL,
  `Cambio_ganador` varchar(3) NOT NULL,
  `lugar_partido` varchar(30) NOT NULL,
  `fecha_realizacion` date NOT NULL,
  `tiros_fallados_2_local` varchar(3) NOT NULL,
  `tiros_fallados_2_visitante` varchar(3) NOT NULL,
  `tiros_fallados_3_local` varchar(3) NOT NULL,
  `tiros_fallados_3_visitante` varchar(3) NOT NULL,
  `tiros_libres_fallados_local` varchar(3) NOT NULL,
  `tiros_libres_fallados_visitante` varchar(3) NOT NULL,
  `tiros_3_local` varchar(55) NOT NULL,
  `tiros_3_visitante` varchar(55) NOT NULL,
  `tiros_2_local` varchar(3) NOT NULL,
  `tiros_2_visitante` varchar(3) NOT NULL,
  `tiro_libre_local` varchar(3) NOT NULL,
  `tiro_libre_visitante` varchar(3) NOT NULL,
  `rebotes_local` varchar(3) NOT NULL,
  `rebotes_visitante` varchar(3) NOT NULL,
  `aisitencias_local` varchar(55) NOT NULL,
  `aisitencias_visitante` varchar(55) NOT NULL,
  `faltas_local` varchar(3) NOT NULL,
  `faltas_visitante` varchar(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `partidos`
--

INSERT INTO `partidos` (`id`, `id_liga`, `id_local`, `id_visitante`, `Marcador_local`, `Marcador_visitante`, `ventaja_maxima_local`, `ventaja_mmaxima_visitante`, `Cambio_ganador`, `lugar_partido`, `fecha_realizacion`, `tiros_fallados_2_local`, `tiros_fallados_2_visitante`, `tiros_fallados_3_local`, `tiros_fallados_3_visitante`, `tiros_libres_fallados_local`, `tiros_libres_fallados_visitante`, `tiros_3_local`, `tiros_3_visitante`, `tiros_2_local`, `tiros_2_visitante`, `tiro_libre_local`, `tiro_libre_visitante`, `rebotes_local`, `rebotes_visitante`, `aisitencias_local`, `aisitencias_visitante`, `faltas_local`, `faltas_visitante`) VALUES
(1, 1, 1, 3, '178', '164', '25', '8', '12', 'Spales Center', '2020-05-06', '12', '24', '55', '50', '1', '2', '35', '27', '33', '39', '7', '5', '64', '68', '42', '43', '9', '6'),
(2, 1, 4, 2, '0', '0', '0', '0', '0', 'Municipal', '2020-07-17', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `telefono`
--

CREATE TABLE `telefono` (
  `id` int(11) NOT NULL,
  `id_jugadores` int(11) NOT NULL,
  `telefono` varchar(13) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `telefono`
--

INSERT INTO `telefono` (`id`, `id_jugadores`, `telefono`) VALUES
(1, 1, '000000001'),
(2, 2, '000000002'),
(3, 3, '000000003'),
(4, 4, '000000004'),
(5, 5, '000000005'),
(6, 6, '000000006'),
(7, 7, '000000007'),
(8, 8, '000000008'),
(9, 9, '000000009'),
(10, 10, '000000010'),
(11, 11, '000000011'),
(12, 12, '000000012'),
(13, 13, '000000013'),
(14, 14, '000000014'),
(15, 15, '000000015'),
(16, 16, '000000016'),
(17, 17, '000000017'),
(18, 18, '000000018'),
(19, 19, '000000019'),
(20, 20, '000000020'),
(21, 21, '000000021'),
(22, 22, '000000022'),
(23, 23, '000000023'),
(24, 24, '000000024'),
(25, 25, '000000025'),
(26, 26, '000000026'),
(27, 27, '000000027'),
(28, 28, '000000028'),
(29, 29, '000000029'),
(30, 30, '000000030'),
(31, 31, '000000031'),
(32, 32, '000000032'),
(33, 33, '000000033'),
(34, 34, '000000034'),
(35, 35, '000000035'),
(36, 36, '000000036'),
(37, 37, '000000037'),
(38, 38, '000000038'),
(39, 39, '000000039'),
(40, 40, '000000040'),
(41, 41, '000000041'),
(42, 42, '000000042'),
(43, 43, '000000043'),
(44, 44, '000000044'),
(45, 45, '000000045'),
(46, 46, '000000046'),
(47, 47, '000000047'),
(48, 48, '000000048'),
(49, 49, '000000049'),
(50, 50, '000000050'),
(51, 51, '000000051'),
(52, 52, '000000052'),
(53, 53, '000000053'),
(54, 54, '000000054'),
(55, 55, '000000055'),
(56, 56, '000000056'),
(57, 57, '000000057'),
(58, 58, '000000058'),
(59, 59, '000000059'),
(60, 60, '000000060');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `clubs`
--
ALTER TABLE `clubs`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `entrenadores`
--
ALTER TABLE `entrenadores`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `equipos`
--
ALTER TABLE `equipos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_equipo_entrenadres` (`id_entrenadores`),
  ADD KEY `FK_equipos_clubs_id` (`id_clubs`),
  ADD KEY `FK_equipos_ligas_id` (`id_ligas`);

--
-- Indices de la tabla `jugadores`
--
ALTER TABLE `jugadores`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_equipo_jugadores` (`id_equipos`);

--
-- Indices de la tabla `ligas`
--
ALTER TABLE `ligas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `partidos`
--
ALTER TABLE `partidos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_partidos_equipo_local` (`id_local`),
  ADD KEY `FK_partidos_equipo_visitante` (`id_visitante`),
  ADD KEY `FK_partidos_liga_id` (`id_liga`);

--
-- Indices de la tabla `telefono`
--
ALTER TABLE `telefono`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_telefono_jugadores` (`id_jugadores`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `clubs`
--
ALTER TABLE `clubs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `entrenadores`
--
ALTER TABLE `entrenadores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `equipos`
--
ALTER TABLE `equipos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `jugadores`
--
ALTER TABLE `jugadores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT de la tabla `ligas`
--
ALTER TABLE `ligas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `telefono`
--
ALTER TABLE `telefono`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `equipos`
--
ALTER TABLE `equipos`
  ADD CONSTRAINT `FK_equipo_entrenadres` FOREIGN KEY (`id_entrenadores`) REFERENCES `entrenadores` (`id`),
  ADD CONSTRAINT `FK_equipos_clubs_id` FOREIGN KEY (`id_clubs`) REFERENCES `clubs` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_equipos_ligas_id` FOREIGN KEY (`id_ligas`) REFERENCES `ligas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `jugadores`
--
ALTER TABLE `jugadores`
  ADD CONSTRAINT `FK_equipo_jugadores` FOREIGN KEY (`id_equipos`) REFERENCES `equipos` (`id`);

--
-- Filtros para la tabla `partidos`
--
ALTER TABLE `partidos`
  ADD CONSTRAINT `FK_partidos_equipo_local` FOREIGN KEY (`id_local`) REFERENCES `equipos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_partidos_equipo_visitante` FOREIGN KEY (`id_visitante`) REFERENCES `equipos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_partidos_liga_id` FOREIGN KEY (`id_liga`) REFERENCES `ligas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `telefono`
--
ALTER TABLE `telefono`
  ADD CONSTRAINT `FK_telefono_jugadores` FOREIGN KEY (`id_jugadores`) REFERENCES `jugadores` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
