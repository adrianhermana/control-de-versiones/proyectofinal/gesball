<?php

use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'Gestball';
?>
	<div class="main-top" id="home">
		<div class="banner_w3lspvt">
				<li>
						<div class="container">
							<div class="w3ls_banner_txt text-center ml-auto pr-xl-0 pr-sm-4 pr-5">
								<h3 class="w3ls_pvt-title text-wh text-uppercase let mb-1">GALERIA DE FOTOS</h3>
								<p>GESTBALL</p>	
							</div>
						</div>
				</li>
		</div>
	</div>

        <div class="container" align="center">
        <div class="text-center  pr-xl-0 pr-sm-2 pr-2">
        <h1><?= Html::a('ENTRAR', ['site/gallery']) ?></h1>
        </div>
        </div>


