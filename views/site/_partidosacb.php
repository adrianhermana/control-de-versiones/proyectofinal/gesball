<?php

use yii\helpers\ArrayHelper;
?>
<div style="padding: 7px;margin-bottom: 100px;">
    <div class="partidoresultado">
        <h1><?= $model->local->nombre ?> <?= $model->Marcador_local ?>  -  <?= $model->Marcador_visitante ?> <?= $model->visitante->nombre ?></h1>
    </div>
    <div class="partidolugar">
        <h3><?= $model->lugar_partido ?></h3>
        <h3><?= $model->fecha_realizacion ?></h3>
    </div>
</div>
