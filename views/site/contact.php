<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;


$this->title = 'Contact';
?>
	<section class="contact py-5">
		<div class="container py-xl-5 py-lg-3">
			<h3 class="title-w3 text-bl text-center font-weight-bold mb-sm-5 mb-4">CONTACTO</h3>
			<!-- mail -->
			<div class="row mail-w3l-agile pt-xl-4">
				<div class="col-md-5 contact-left-w3ls">
					<h3>INFO</h3>
					<div class="row mail-w3 my-4">
						<div class="col-md-2 col-sm-2 col-2 contact-icon-wthree">
							<?= Html::img('@web/images/email.jpg', ['alt' => 'My logo'], ['class' => 'img-fluid']) ?>
						</div>
						<div>
							<h3 class="title-w3 font-weight mb-sm-0 mb-1">EMAIL</h3>
							<p><a href="mailto:info@example.com">gestball@gestball.com</a></p>
						</div>
					</div>
                    <div class="map p-2">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d100949.24429313939!2d-122.44206553967531!3d37.75102885910819!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80859a6d00690021%3A0x4a501367f076adff!2sSan+Francisco%2C+CA%2C+USA!5e0!3m2!1sen!2sin!4v1472190196783"
                        class="map" style="border:0" allowfullscreen=""></iframe>
                    </div>
				</div>
				<div class="col-md-6 agileinfo_mail_grid_right mt-md-0 mt-5">
					<form action="#" method="post">
						<div class="form-group">
							<input type="text" name="Name" class="form-control" placeholder="Nombre" required="">
						</div>
						<div class="form-group">
							<input type="email" name="Email" class="form-control" placeholder="Email" required="">
						</div>
						<div class="form-group">
							<textarea name="Message" placeholder="Mensaje......." required=""></textarea>
						</div>
						<h1><?= Html::a('ENVIAR', ['site/confirmado']) ?></h1>
                                                
					</form>
				</div>
			</div>
		</div>
	</section>

