<?php
use yii\helpers\Html;
use yii\helpers\BaseHtml;
use yii\widgets\ActiveForm;
use yii\db\Command;
use yii\grid\GridView;
use yii\widgets\ListView;
$this->title = 'Gestball';
?>
	<div class="main-top" id="home">
		<div class="banner_w3lspvt">
			<div class="csslider infinity" id="slider5">
				<ul class="banner_slide_bg">
					<li>
						<div class="container">
							<div class="w3ls_banner_txt text-center ml-auto pr-xl-0 pr-sm-4 pr-5">
								<h3 class="w3ls_pvt-title text-wh text-uppercase let mb-1">GESTBALL</h3>
								<p>ESTADISTICAS DE BALONCESTO</p>	
							</div>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="stats py-0" id="stats">
		<div class="container py-xl-5 py-lg-3">
			<div class="row text-center py-sm-4">        
                <div class="col-md-3 col-sm-6 w3layouts_stats_left mt-md-0 mt-4">
					<p class="counter"><?=$num_jugadores ?></p>
					<p class="para-text-w3ls text-li">JUGADORES</p>
				</div>
				<div class="col-md-3 col-sm-6 w3layouts_stats_left mt-md-0 mt-4">
					<p class="counter"><?=$num_ligas ?></p>
					<p class="para-text-w3ls text-li">LIGAS</p>
				</div>
				<div class="col-md-3 col-sm-6 w3layouts_stats_left mt-md-0 mt-4">
					<p class="counter"><?=$num_equipos ?></p>
					<p class="para-text-w3ls text-li">EQUIPOS</p>
			</div>
            <div class="col-md-3 col-sm-6 w3layouts_stats_left mt-md-0 mt-4">
					<p class="counter"><?=$num_clubs ?></p>
					<p class="para-text-w3ls text-li">CLUBS</p>
			</div>
		</div>
	</div>
</div>
	<div class="what py-5">
		<div class="container py-xl-5 py-lg-3">
	s		<h3 class="title-w3 text-bl text-center font-weight mb-sm-1 mb-4">Gestball es una aplicación de estadísticas, se utiliza principalmente en las competiciones cadete, junior y sénior </h3>
		</div>
	</div>
	<section class="testimonials py-5" id="testimonials">
			<div class="news-grids text-center">
				<div class="row news-grids text-center">
					<div class="col-md-4 gal-img">
						<a href="#gal1"><?= Html::img('@web/images/g1.jpg', ['alt'=>'Popup Image'], ['class'=>'img-fluid']);?></a>
						<a href="#gal2"><?= Html::img('@web/images/g2.jpg', ['alt'=>'Popup Image'], ['class'=>'img-fluid mt-4']);?></a>
					</div>
					<div class="col-md-4 gal-img my-md-0 my-4">
                                         <a href="#gal3"><?= Html::img('@web/images/g4.jpg', ['alt'=>'Popup Image'], ['class'=>'img-fluid']);?></a>
					</div>
					<div class="col-md-4 gal-img">
						<a href="#gal5"><?= Html::img('@web/images/g5.jpg', ['alt'=>'Popup Image'], ['class'=>'img-fluid']);?></a>
						<a href="#gal7"><?= Html::img('@web/images/g3.jpg', ['alt'=>'Popup Image'], ['class'=>'img-fluids mt-4']);?></a>
					</div>
				</div>
			</div>
	<div id="gal1" class="popup-effect animate">
		<div class="popup">
			<?= Html::img('@web/images/g1.jpg', ['alt'=>'Popup Image'], ['class'=>'img-fluid']);?>
                  <a class="close" href="#gallery">&times;</a>
		</div>
	</div>

	<div id="gal2" class="popup-effect animate">
		<div class="popup">
			<?= Html::img('@web/images/g2.jpg', ['alt'=>'Popup Image'], ['class'=>'img-fluid']);?>
            <a class="close" href="#gallery">&times;</a>
		</div>
	</div>
	<div id="gal3" class="popup-effect animate">
		<div class="popup">
			<?= Html::img('@web/images/g4.jpg', ['alt'=>'Popup Image'], ['class'=>'img-fluid']);?>
            <a class="close" href="#gallery">&times;</a>
		</div>
	</div>
	<div id="gal7" class="popup-effect animate">
		<div class="popup">
			<?= Html::img('@web/images/g3.jpg', ['alt'=>'Popup Image'], ['class'=>'img-fluid']);?>
            <a class="close" href="#gallery">&times;</a>
		</div>
	</div>
	<div id="gal5" class="popup-effect animate">
		<div class="popup">
			<?= Html::img('@web/images/g5.jpg', ['alt'=>'Popup Image'], ['class'=>'img-fluid']);?>
            <a class="close" href="#gallery">&times;</a>
		</div>
	</div>
  </section> 
    <div class="what py-5">
		<div class="container py-xl-5 py-lg-3">
			<h2 class="title-w3 text-bl text-center font-weight mb-sm-1 mb-4">Esta herramienta permite la recopilación de datos de un partido de baloncesto al instante y de forma oficial convirtiéndola en más práctica y sencilla por los oficiales de mesa. Su implantación permite disponer de los datos del partido a cualquier persona a través de la zona de competiciones de nuestra página web, así como de las estadísticas de cada equipo y por jugador</h2>
		</div>
	</div>
        <section class="testimonials py-5" id="testimonials">
		<div class="container py-xl-5 py-lg-3">
			<h3 class="title-w3 text-bl text-center font-weight-bold mb-sm-5 mb-4">Una nueva experiencia deportiva</h3>
			
				<div class="serv_bottom text-center pt-xl-4">
					<div class="row mt-xl-4">
						<div class="col-lg-3 col-md-6 col-sm-6">
							<div class="bottom-gd">
                                                            <?= Html::img('@web/images/wh1.jpg', ['alt'=>'Popup Image'], ['class'=>'img-fluid']);?>
								<h3 class="my-3 font-weight-bold">WEB DESIGN</h3>
								<p>Cómodo diseño</p>
							</div>
						</div>
						<div class="col-lg-3 col-md-6 col-sm-6">
							<div class="bottom-gd">
								<?= Html::img('@web/images/wh2.jpg', ['alt'=>'Popup Image'], ['class'=>'img-fluid']);?>
								<h3 class="my-3 font-weight-bold">DATOS</h3>
								<p>Sigue en todas partes tus estadísticas</p>
							</div>
						</div>
						<div class="col-lg-3 col-md-6 col-sm-6 mt-md-0 mt-4">
							<div class="bottom-gd">
								<?= Html::img('@web/images/wh3.jpg', ['alt'=>'Popup Image'], ['class'=>'img-fluid']);?>
								<h3 class="my-3 font-weight-bold">24/7</h3>
								<p>En funcionamiento 24h los 7</p>
							</div>
						</div>
                        <br/><br/><br/><br/>
						<div class="col-lg-3 col-md-6 col-sm-6 mt-md-0 mt-4">
							<div class="bottom-gd">
								<?= Html::img('@web/images/wh4.jpg', ['alt'=>'Popup Image'], ['class'=>'img-fluid']);?>
								<h3 class="my-3 font-weight-bold">CONSULTA ESTADISTICAS</h3>
								<p>Conslta las estadísticas de tu equipo, de otros equipos o jugadores</p>
							</div>
						</div>
					</div>
				</div>
		</div>
        </section>
    <div class="what py-5">
	<div class="container py-xl-5 py-lg-3">
			<h3 class="title-w3 text-bl text-center font-weight-bold mb-sm-5 mb-4">CONTACTO</h3>
			<!-- mail -->
			<div class="row mail-w3l-agile pt-xl-4">
				<div class="col-md-5 contact-left-w3ls">
					<h3>INFO</h3>
					<div class="row mail-w3 my-4">
						<div class="col-md-2 col-sm-2 col-2 contact-icon-wthree">
							<?= Html::img('@web/images/email.jpg', ['alt' => 'My logo'], ['class' => 'img-carta']) ?>
						</div>
						<div>
							<h3 class="title-w3 font-weight mb-sm-0 mb-1">EMAIL</h3>
							<p><a href="mailto:info@example.com">gestball@gestball.com</a></p>
						</div>
					</div>
				</div>
				<div class="col-md-6 agileinfo_mail_grid_right mt-md-0 mt-5">
					<form action="#" method="post">
						<div class="form-group">
							<input type="text" name="Name" class="form-control" placeholder="Nombre" required="">
						</div>
						<div class="form-group">
							<input type="email" name="Email" class="form-control" placeholder="Email" required="">
						</div>
						<div class="form-group">
							<textarea name="Message" placeholder="Mensaje......." required=""></textarea>
						</div>
						<h1><?= Html::a('ENVIAR', ['site/confirmado']) ?></h1>
					</form>
				</div>
			</div>
		</div>
    </div>
<a href="#home" class="move-top text-center"></a>
    