<?php

use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'Gestball';
?>
			<div class="news-grids text-center">
				<div class="row news-grids text-center">
					<div class="col-md-4 gal-img">
						<a href="#gal1"><?= Html::img('@web/images/g1.jpg', ['alt'=>'some', 'class'=>'img-fluids']);?></a>
						<a href="#gal2"><?= Html::img('@web/images/g2.jpg', ['alt'=>'some', 'class'=>'img-fluids mt-4']);?></a>
					</div>
					<div class="col-md-4 gal-img my-md-0 my-4">
                                        <a href="#gal3"><?= Html::img('@web/images/g4.jpg', ['alt'=>'some', 'class'=>'img-fluids']);?></a>
					</div>
					<div class="col-md-4 gal-img">
						<a href="#gal5"><?= Html::img('@web/images/g3.jpg', ['alt'=>'some', 'class'=>'img-fluids']);?></a>
						<a href="#gal7"><?= Html::img('@web/images/g5.jpg', ['alt'=>'some', 'class'=>'img-fluids mt-4']);?></a>
					</div>
				</div>
			</div>
	<div id="gal1" class="popup-effect animate">
		<div class="popup">
			<?= Html::img('@web/images/g1.jpg', ['alt'=>'some', 'class'=>'img-fluids']);?>
            <a class="close" href="#gallery">&times;</a>
		</div>
	</div>
	<div id="gal2" class="popup-effect animate">
		<div class="popup">
			<?= Html::img('@web/images/g2.jpg', ['alt'=>'some', 'class'=>'img-fluids']);?>
            <a class="close" href="#gallery">&times;</a>
		</div>
	</div>
	<div id="gal3" class="popup-effect animate">
		<div class="popup">
			<?= Html::img('@web/images/g4.jpg', ['alt'=>'some', 'class'=>'img-fluids']);?>
            <a class="close" href="#gallery">&times;</a>
		</div>
	</div>
	<div id="gal7" class="popup-effect animate">
		<div class="popup">
			<?= Html::img('@web/images/g5.jpg', ['alt'=>'some', 'class'=>'img-fluids']);?>
            <a class="close" href="#gallery">&times;</a>
		</div>
	</div>
	<div id="gal5" class="popup-effect animate">
		<div class="popup">
			<?= Html::img('@web/images/g3.jpg', ['alt'=>'some', 'class'=>'img-fluids']);?>
            <a class="close" href="#gallery">&times;</a>
		</div>
	</div>

