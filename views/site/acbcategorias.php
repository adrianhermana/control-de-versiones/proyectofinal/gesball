<?php
 
use yii\helpers\Html;
use yii\helpers\BaseHtml;
use yii\widgets\ActiveForm;

?>
	<div class="temporada row">
         <div class="temporada3" >
        <?= Html::img('@web/images/logo.png', ['alt'=>'logo']);?>
            </div>
            <h1 class="temporada2">TEMPORADA 2019 / 2020</h1>     <h1 class="volver"><?= Html::a(' 🡸 ', ['site/liga']) ?></h1>                  	
    </div>

    <div class="categoria1">
        <div class="categoria">
                <h1 class="grupales">CATEGORIA</h1>
        </div>   
        <div class="ligaactual">
                <h1 class="ligaeba">ACB</h1>
        </div>
        <div class="ligaactualimagen">
                <div class="ligaactualimagen2">
                    <?= Html::img('@web/images/Imagen_6.png', ['alt'=>'logo']);?>
                </div>
        </div>
        <div class="grupos">
                    <h1 class="ligaeba">GRUPOS 2</h1>
        </div>
        <div class="equipos">
                    <h1 class="ligaeba">CEINMARK</h1>
        </div>
        <div class="grupo2">
                <h1 class="grupales2">GRUPOS</h1>
        </div>   
        
        <div class="selgrupos">
            <div class="ligaacb4">
                <div class="imagen1liga">
                    <?= Html::img('@web/images/Imagen_6.png', ['alt'=>'logo']);?>
                </div>
                <div class="ligaacbentrar">
                    <h1 class="ligasespa"><?= Html::a('LIGA ACB', ['site/partidosacb']) ?></h1>
                </div>
           </div>
           <div class="ligaacb5">
                <div class="imagen1liga">
                    <?= Html::img('@web/images/Imagen_6.png', ['alt'=>'logo']);?>
                </div>
                <div class="ligaacbentrar">
                    <h1 class="ligasespa"><?= Html::a('AMISTOSOS ACB', ['site/amistososacb']) ?></h1>
                </div>
           </div>
            
            
                
        </div> 
    </div>    
