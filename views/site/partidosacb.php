<?php

use yii\helpers\Html;
use yii\helpers\BaseHtml;
use yii\widgets\ActiveForm;
use yii\db\Command;
use yii\grid\GridView;
use yii\widgets\ListView;

$equipos = $num * 2;
    
    
?>
<div class="temporada row">
    <div class="temporada3" >
        <?= Html::img('@web/images/logo.png', ['alt' => 'logo']); ?>
    </div>
    <h1 class="temporada2">TEMPORADA 2019 / 2020</h1>  
    <h1 class="volver"><?= Html::a(' 🡸 ', ['site/acbcategorias']) ?></h1> 
</div>

<div class="categoria1">
    <div class="categoria">
        <h1 class="grupales">CATEGORIA</h1>
    </div>   
    <div class="ligaactual">
        <h1 class="ligaeba">ACB</h1>
    </div>
    <div class="ligaactual2">
        <h1 class="ligaeba">LIGA</h1>
    </div>
    <div class="ligaactualimagen4">
        <div class="ligaactualimagen2">
            <?= Html::img('@web/images/Imagen_6.png', ['alt' => 'logo']); ?>
        </div>
    </div>
    <div class="grupos">
        <h1 class="ligaeba">PARTIDOS <?=$num?></h1>
    </div>
    <div class="equipos">
        <h1 class="ligaeba">EQUIPOS <?=$equipos?></h1>
    </div>
    <div class="partidos">
        <h1 class="grupales2">PARTIDOS</h1>
    </div>   

    <div class="selpartidos">
        <div class="partido1">
                <div class="partidoinfo">
                    <h1 style="color: white"><?= Html::a('VER', ['site/partido1']) ?></h1>
                </div>
                   <?= ListView::widget([
                   'dataProvider' => $dataProvider,
                   'itemView' =>'_partidosacb',
                   'layout' => "\n{items}",
                      ]);
                  ?>
        </div>
        <div class="partido2">
                <div class="partidoinfo">
                   <h1 style="color: white"><?= Html::a('VER', ['site/partido2']) ?></h1>
                </div>
        </div>  
    </div> 
    <div class="clasificacion">
        <h1 class="grupales2">PATROCINADOR</h1>
    </div>   
    <div class="clasificacion2">
            <div class="ligaactualimagen3">
            <?= Html::img('@web/images/unnamed.png', ['alt' => 'logo']); ?>    
        </div>
    </div> 
</div>    
