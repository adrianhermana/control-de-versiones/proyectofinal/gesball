<?php

use yii\helpers\Html;
use yii\helpers\BaseHtml;
use yii\widgets\ActiveForm;
use yii\db\Command;
use yii\grid\GridView;
use yii\widgets\ListView;

$numero_t2_local = $localt2anotados + $localt2fallados; 
$numero_t3_local = $localt3anotados + $localt3fallados; 
$numero_tl_local = $localtlanotados + $localtlfallados; 
$numero_t2_visitante = $visitantet2anotados + $visitantet2fallados; 
$numero_tl_visitante = $visitantetlanotados + $visitantetlfallados; 
$numero_t3_visitante = $visitantet3anotados + $visitantet3fallados;

if ( $numero_t2_local > 0){
    $localt2 = round(($localt2anotados*100)/$numero_t2_local);
}else{
    $localt2 = 0;
}
if ( $numero_t3_local > 0){
    $localt3 = round(($localt3anotados*100)/$numero_t3_local);
}else{
    $localt3 = 0;
}
if ( $numero_tl_local > 0){
    $localtl = round(($localtlanotados*100)/$numero_tl_local);
}else{
    $localtl = 0;
}
if ( $numero_t2_visitante > 0){
    $visitantet2 = round(($visitantet2anotados*100)/$numero_t2_visitante);
}else{
    $visitantet2 = 0;
}
if ( $numero_t3_visitante > 0){
    $visitantet3 = round(($visitantet3anotados*100)/$numero_t3_visitante);
}else{
    $visitantet3 = 0;
}
if ( $numero_tl_visitante > 0){
    $visitantetl = round(($visitantetlanotados*100)/$numero_tl_visitante);
}else{
    $visitantetl = 0;
}
?>
	<div class="temporada row">
         <div class="temporada3" >
        <?= Html::img('@web/images/logo.png', ['alt'=>'logo']);?>
            </div>  
            <h1 class="temporada2"> <?=$nombre_liga?> - JORNADA 1</h1>  <h1 class="volver"><?= Html::a(' 🡸 ', ['site/partidosacb']) ?></h1>                 
        </div>
    <div class="equipo row">
        <div class="equipo1logo row">
              <?= Html::img('@web/images/cantbasket.png', ['alt'=>'Popup Image'], ['class'=>'img-fluid']);?>   	
        </div>
        <div class="nombreequipolocal row">
            <h1><?=$nombre_local?></h1>           	
        </div>
        <div class="resultadolocal row">
            <h1><?=$mar_local?></h1>          	
        </div>
        <div class="vs row">
            <h1>VS</h1>            
        </div>
        <div class="equipo2logo row">
              <?= Html::img('@web/images/cantbasket.png', ['alt'=>'Popup Image'], ['class'=>'img-fluid']);?>   		
        </div>    
         <div class="nombreequipovisitante row">
            <h1><?=$nombre_visitante?></h1>         	
        </div>
        <div class="resultadovisitante row">
               <h1><?=$mar_visitante?></h1>
        </div>  
    </div> 
    <div class="estadisticas row">
        <div class="estadisticas1 row">
            <div>
                    <h1>T2<br/>
                    <h3><?=$localt2?>%</h3>
                <progress id="html5" max="100" value="<?=$localt2?>"></progress>
			 <span></span>
            </div>
        </div>
        <div class="estadisticas2 row">
            <div>
                    <h1>T3</h1>
                    <h3><?=$localt3?>%</h3>
                <progress id="html5" max="100" value="<?=$localt3?>"></progress>
			 <span></span>
            </div>
        </div>
        <div class="estadisticas3 row">
            <div>
                    <h1>TL</h1>
                    <h3><?=$localtl?>%</h3>
                <progress id="html5" max="100" value="<?=$localtl?>"></progress>
			 <span></span>
            </div>
        </div>
        <div class="estadisticases row">
            <h1>ETADISTICAS</h1>
        </div>
        <div class="estadisticas4 row">
            <div>
                    <h1>TL</h1>
                    <h3><?=$visitantetl?>%</h3>
                <progress id="html5" max="100" value="<?=$visitantetl?>"></progress>
			 <span></span>
            </div>
        </div>
        <div class="estadisticas5 row">
            <div>
                    <h1>T3</h1>
                    <h3><?=$visitantet3?>%</h3>
                <progress id="html5" max="100" value="<?=$visitantet3?>"></progress>
			 <span></span>
            </div>
        </div>
        <div class="estadisticas6 row">
            <div>
                    <h1>T2</h1>
                    <h3><?=$visitantet2?>%</h3>
                <progress id="html5" max="100" value="<?=$visitantet2?>"></progress>
			 <span></span>
            </div>
        </div>
    </div>    
    <div class="ventajas row">
         <div class="estadisticas2 row">
            <div>
                    <h1><?=$asitenciaslocal?> / <?=$asitenciasvisitante?></h1>
                    <h1>ASISTENCIAS</h1>
            </div>
        </div>
         <div class="estadisticases row">
            <div>
                    <h1><?=$ventajalocal?> / <?=$ventajavisitante?></h1>
                    <h1>MAYOR VENTAJA</h1>
            </div>
        </div>
        <div class="estadisticasextra row">
            <div>
                    <h1><?=$cambio?></h1>
                    <h1>CAMBIO DE GANADOR</h1>
            </div>
        </div>       
    </div>




<a href="#home" class="move-top text-center"></a>
    
    
