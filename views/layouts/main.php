<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8" />
    <meta name="keywords" content="GESTBALL" />
    <script>
		addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
    </script>
    <link href="//fonts.googleapis.com/css?family=Nunito+Sans:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i&amp;subset=latin-ext,vietnamese"
    rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese"
	 rel="stylesheet">
</head>
<body>
<?php $this->beginBody() ?>
<div class="wrap">
    <div class="container-fluid">
                <div class="header d-md-flex justify-content-between align-items-center py-3 px-xl-5 px-lg-3 px-2">
                    <div id="logo">
                        <h1><?= Html::a('GESTBALL', ['site/index']) ?></h1> 
                    </div>
                    <div class="nav_w3ls">
                        <nav>
                            <label for="drop" class="toggle">Menu</label>
                            <input type="checkbox" id="drop" />
                            <ul class="menu">
                                <li class="mx-lg-4 mx-md-3 my-md-0 my-2"><?= Html::a('Resultado', ['site/resultado']) ?></li>
                                <li class="mx-lg-4 mx-md-3 my-md-0 my-2"><?= Html::a('Galeria', ['site/entrargallery']) ?></li>
                                <li class="mx-lg-4 mx-md-3 my-md-0 my-2"><?= Html::a('Contacto', ['site/contact']) ?></li>
                                <li class="mx-lg-4 mx-md-3 my-md-0 my-2"><?= Html::a('Login', ['site/iniciarsesion']) ?></li>
                            </ul>
                        </nav>
                    </div>
                </div>
     </div>

    <div class="">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>
<footer class="footer">
    <div class="cpy-right text-center py-3">
        <p>© 2020 GESTBALL.</p>
</div>
</footer>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
