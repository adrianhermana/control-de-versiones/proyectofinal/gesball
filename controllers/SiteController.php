<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\SqlDataProvider;
use yii\data\ActiveDataProvider;
use app\models\Clubs;
use app\models\Entrenadores;
use app\models\Equipos;
use app\models\Jugadores;
use app\models\Ligas;
use app\models\Partidos;
use app\models\Telefono;

class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
       $num = Jugadores::find()->count();
       $numligas = Ligas::find()->count();
       $numequipos = Equipos::find()->count();
       $numclubs = Clubs::find()->count();
        $dataProvider = new ActiveDataProvider([
            'query' => Jugadores::find() ,
    ]);
        return $this->render('index',[
            'dataProvider'=>$dataProvider,
            'num_jugadores'=>$num,
            'num_ligas'=>$numligas,
            'num_equipos'=>$numequipos,
            'num_clubs'=>$numclubs,
        ]);
    }
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
    public function actionContact()
    {
        return $this->render('contact');
    }
     public function actionGallery()
    {
        return $this->render('gallery');
    }
     public function actionEntrargallery()
    {
        return $this->render('entrargallery');
    }
     public function actionIniciarsesion()
    {
        return $this->render('iniciarsesion');
    }
     public function actionResultado()
    {
        return $this->render('resultado');
    }
     public function actionLiga()
    {
        return $this->render('liga');
    }
    public function actionLiga2()
    {
        return $this->render('liga2');
    }
     public function actionAcbcategorias()
    {
        return $this->render('acbcategorias');
    }   
     public function actionLeborocategorias()
    {
        return $this->render('leborocategorias');
    }   
    public function actionConfirmado()
    {
        return $this->render('confirmado');
    }   
         public function actionAmistososacb()
    {
        return $this->render('amistososacb');
    }   
     public function actionPartido1()
    {
        $dataProvider = Partidos::find()->select("id_liga")->where("id='1'");
        $nombreLiga = Ligas::findOne($dataProvider)->nombre;
        $equiposlocal = Partidos::find()->select("id_local")->where("id='1'");
        $local = Equipos::findOne($equiposlocal)->nombre;
        $equiposvisitante = Partidos::find()->select("id_visitante")->where("id='1'");
        $visitante = Equipos::findOne($equiposvisitante)->nombre;
        $marcadorlocal = Partidos::findOne(1)->Marcador_local;
        $marcadorvisitante = Partidos::findOne(1)->Marcador_visitante;
        $cambioganador = Partidos::findOne(1)->Cambio_ganador;
        $asistencias1 = Partidos::findOne(1)->aisitencias_local;
        $asistencias2 = Partidos::findOne(1)->aisitencias_visitante;
        $ventaja1 = Partidos::findOne(1)->ventaja_maxima_local;
        $ventaja2 = Partidos::findOne(1)->ventaja_mmaxima_visitante;
        $localt2v = Partidos::findOne(1)->tiros_2_local;
        $localt2f = Partidos::findOne(1)->tiros_fallados_2_local;
        $localt3v = Partidos::findOne(1)->tiros_3_local;
        $localt3f = Partidos::findOne(1)->tiros_fallados_3_local;
        $localtlv = Partidos::findOne(1)->tiro_libre_local;
        $localtlf = Partidos::findOne(1)->tiros_libres_fallados_local;
        $visitantet2v = Partidos::findOne(1)->tiros_2_visitante;
        $visitantet2f = Partidos::findOne(1)->tiros_fallados_2_visitante;
        $visitantet3v = Partidos::findOne(1)->tiros_3_visitante;
        $visitantet3f = Partidos::findOne(1)->tiros_fallados_3_visitante;
        $visitantetlv = Partidos::findOne(1)->tiro_libre_visitante;
        $visitantetlf = Partidos::findOne(1)->tiros_libres_fallados_visitante;
        
        return $this->render("partido1",[
            "nombre_liga"=>$nombreLiga,
            "nombre_local"=>$local,
           "nombre_visitante"=>$visitante,
            "mar_local"=>$marcadorlocal,
            "mar_visitante"=>$marcadorvisitante,
            "cambio"=>$cambioganador,
            "asitenciaslocal"=>$asistencias1,
            "asitenciasvisitante"=>$asistencias2,
            "ventajalocal"=>$ventaja1,
            "ventajavisitante"=>$ventaja2,
            "localt2anotados"=>$localt2v,
            "localt2fallados"=>$localt2f,
            "localt3anotados"=>$localt3v,
            "localt3fallados"=>$localt3f,
            "localtlanotados"=>$localtlv,
            "localtlfallados"=>$localtlf,
            "visitantet2anotados"=>$visitantet2v,
            "visitantet2fallados"=>$visitantet2f,
            "visitantet3anotados"=>$visitantet3v,
            "visitantet3fallados"=>$visitantet3f,
            "visitantetlanotados"=>$visitantetlv,
            "visitantetlfallados"=>$visitantetlf,
        ]);
    }
    public function actionPartido2()
    {
        $dataProvider = Partidos::find()->select("id_liga")->where("id='2'");
        $nombreLiga = Ligas::findOne($dataProvider)->nombre;
        $equiposlocal = Partidos::find()->select("id_local")->where("id='2'");
        $local = Equipos::findOne($equiposlocal)->nombre;
        $equiposvisitante = Partidos::find()->select("id_visitante")->where("id='2'");
        $visitante = Equipos::findOne($equiposvisitante)->nombre;
        $marcadorlocal = Partidos::findOne(2)->Marcador_local;
        $marcadorvisitante = Partidos::findOne(2)->Marcador_visitante;
        $cambioganador = Partidos::findOne(2)->Cambio_ganador;
        $asistencias1 = Partidos::findOne(2)->aisitencias_local;
        $asistencias2 = Partidos::findOne(2)->aisitencias_visitante;
        $ventaja1 = Partidos::findOne(2)->ventaja_maxima_local;
        $ventaja2 = Partidos::findOne(2)->ventaja_mmaxima_visitante;
        $localt2v = Partidos::findOne(2)->tiros_2_local;
        $localt2f = Partidos::findOne(2)->tiros_fallados_2_local;
        $localt3v = Partidos::findOne(2)->tiros_3_local;
        $localt3f = Partidos::findOne(2)->tiros_fallados_3_local;
        $localtlv = Partidos::findOne(2)->tiro_libre_local;
        $localtlf = Partidos::findOne(2)->tiros_libres_fallados_local;
        $visitantet2v = Partidos::findOne(2)->tiros_2_visitante;
        $visitantet2f = Partidos::findOne(2)->tiros_fallados_2_visitante;
        $visitantet3v = Partidos::findOne(2)->tiros_3_visitante;
        $visitantet3f = Partidos::findOne(2)->tiros_fallados_3_visitante;
        $visitantetlv = Partidos::findOne(2)->tiro_libre_visitante;
        $visitantetlf = Partidos::findOne(2)->tiros_libres_fallados_visitante;
        
        return $this->render("partido2",[
            "nombre_liga"=>$nombreLiga,
            "nombre_local"=>$local,
           "nombre_visitante"=>$visitante,
            "mar_local"=>$marcadorlocal,
            "mar_visitante"=>$marcadorvisitante,
            "cambio"=>$cambioganador,
            "asitenciaslocal"=>$asistencias1,
            "asitenciasvisitante"=>$asistencias2,
            "ventajalocal"=>$ventaja1,
            "ventajavisitante"=>$ventaja2,
            "localt2anotados"=>$localt2v,
            "localt2fallados"=>$localt2f,
            "localt3anotados"=>$localt3v,
            "localt3fallados"=>$localt3f,
            "localtlanotados"=>$localtlv,
            "localtlfallados"=>$localtlf,
            "visitantet2anotados"=>$visitantet2v,
            "visitantet2fallados"=>$visitantet2f,
            "visitantet3anotados"=>$visitantet3v,
            "visitantet3fallados"=>$visitantet3f,
            "visitantetlanotados"=>$visitantetlv,
            "visitantetlfallados"=>$visitantetlf,
        ]);
    }
    public function actionPartidosacb()
    {
        $num = Partidos::find()->count();
        $equiposlocal1 = Partidos::find()->select("id_local")->where("id='1'");
        $local1 = Equipos::findOne($equiposlocal1)->nombre;
        $equiposvisitante1 = Partidos::find()->select("id_visitante")->where("id='1'");
        $visitante1 = Equipos::findOne($equiposvisitante1)->nombre;
        $equiposlocal2 = Partidos::find()->select("id_local")->where("id='2'");
        $local2 = Equipos::findOne($equiposlocal2)->nombre;
        $equiposvisitante3 = Partidos::find()->select("id_visitante")->where("id='2'");
        $visitante2 = Equipos::findOne($equiposvisitante3)->nombre;
        $localid1 = Partidos::findOne(1)->id_local;
        $localid2 = Partidos::findOne(2)->id_local;
        $visitanteid1 = Partidos::findOne(1)->id_visitante;
        $visitanteid2 = Partidos::findOne(2)->id_visitante;
        
        $dataProvider = new ActiveDataProvider([
            'query' => Partidos::find() -> select("id_local,Marcador_local,Marcador_visitante,id_visitante,lugar_partido,fecha_realizacion"),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        $dataProvider2 = new ActiveDataProvider([
            'query' => Partidos::find() -> select("id_local,Marcador_local,Marcador_visitante,id_visitante"),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
     return $this->render("partidosacb",[
         "dataProvider"=>$dataProvider,
         "dataProvider2"=>$dataProvider2,
         "num"=>$num,
         "nombre_1"=>$local1,
         "nombre_2"=>$visitante1,
         "nombre_3"=>$local2,
         "nombre_4"=>$visitante2,
         "localid1"=>$localid1,
         "localid2"=>$localid2,
         "visitanteid1"=>$visitanteid1,
         "visitanteid2"=>$visitanteid2,
         "campos"=>['id_local','Marcador_local','Marcador_visitante','id_visitante'],

     ]);
    }   

    public function actionIndex1()
    {
     
    }   
    public function actionEquipolocal()
    {
        return $this->render('equipolocal');
    }   
    public function actionAbout()
    {
        return $this->render('about');
    }
}
