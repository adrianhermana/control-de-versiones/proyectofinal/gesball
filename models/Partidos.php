<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "partidos".
 *
 * @property int $id
 * @property int $id_liga
 * @property int $id_local
 * @property int $id_visitante
 * @property string $Marcador_local
 * @property string $Marcador_visitante
 * @property string $ventaja_maxima_local
 * @property string $ventaja_mmaxima_visitante
 * @property string $Cambio_ganador
 * @property string $lugar_partido
 * @property string $fecha_realizacion
 * @property string $tiros_fallados_2_local
 * @property string $tiros_fallados_2_visitante
 * @property string $tiros_fallados_3_local
 * @property string $tiros_fallados_3_visitante
 * @property string $tiros_libres_fallados_local
 * @property string $tiros_libres_fallados_visitante
 * @property string $tiros_3_local
 * @property string $tiros_3_visitante
 * @property string $tiros_2_local
 * @property string $tiros_2_visitante
 * @property string $tiro_libre_local
 * @property string $tiro_libre_visitante
 * @property string $rebotes_local
 * @property string $rebotes_visitante
 * @property string $aisitencias_local
 * @property string $aisitencias_visitante
 * @property string $faltas_local
 * @property string $faltas_visitante
 *
 * @property Equipos $local
 * @property Equipos $visitante
 * @property Ligas $liga
 */
class Partidos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'partidos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'id_liga', 'id_local', 'id_visitante', 'Marcador_local', 'Marcador_visitante', 'ventaja_maxima_local', 'ventaja_mmaxima_visitante', 'Cambio_ganador', 'lugar_partido', 'fecha_realizacion', 'tiros_fallados_2_local', 'tiros_fallados_2_visitante', 'tiros_fallados_3_local', 'tiros_fallados_3_visitante', 'tiros_libres_fallados_local', 'tiros_libres_fallados_visitante', 'tiros_3_local', 'tiros_3_visitante', 'tiros_2_local', 'tiros_2_visitante', 'tiro_libre_local', 'tiro_libre_visitante', 'rebotes_local', 'rebotes_visitante', 'aisitencias_local', 'aisitencias_visitante', 'faltas_local', 'faltas_visitante'], 'required'],
            [['id', 'id_liga', 'id_local', 'id_visitante'], 'integer'],
            [['fecha_realizacion'], 'safe'],
            [['Marcador_local', 'Marcador_visitante', 'ventaja_maxima_local', 'ventaja_mmaxima_visitante', 'Cambio_ganador', 'tiros_fallados_2_local', 'tiros_fallados_2_visitante', 'tiros_fallados_3_local', 'tiros_fallados_3_visitante', 'tiros_libres_fallados_local', 'tiros_libres_fallados_visitante', 'tiros_2_local', 'tiros_2_visitante', 'tiro_libre_local', 'tiro_libre_visitante', 'rebotes_local', 'rebotes_visitante', 'faltas_local', 'faltas_visitante'], 'string', 'max' => 3],
            [['lugar_partido'], 'string', 'max' => 30],
            [['tiros_3_local', 'tiros_3_visitante', 'aisitencias_local', 'aisitencias_visitante'], 'string', 'max' => 55],
            [['id_local'], 'exist', 'skipOnError' => true, 'targetClass' => Equipos::className(), 'targetAttribute' => ['id_local' => 'id']],
            [['id_visitante'], 'exist', 'skipOnError' => true, 'targetClass' => Equipos::className(), 'targetAttribute' => ['id_visitante' => 'id']],
            [['id_liga'], 'exist', 'skipOnError' => true, 'targetClass' => Ligas::className(), 'targetAttribute' => ['id_liga' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_liga' => 'Id Liga',
            'id_local' => 'Id Local',
            'id_visitante' => 'Id Visitante',
            'Marcador_local' => 'Marcador Local',
            'Marcador_visitante' => 'Marcador Visitante',
            'ventaja_maxima_local' => 'Ventaja Maxima Local',
            'ventaja_mmaxima_visitante' => 'Ventaja Mmaxima Visitante',
            'Cambio_ganador' => 'Cambio Ganador',
            'lugar_partido' => 'Lugar Partido',
            'fecha_realizacion' => 'Fecha Realizacion',
            'tiros_fallados_2_local' => 'Tiros Fallados 2 Local',
            'tiros_fallados_2_visitante' => 'Tiros Fallados 2 Visitante',
            'tiros_fallados_3_local' => 'Tiros Fallados 3 Local',
            'tiros_fallados_3_visitante' => 'Tiros Fallados 3 Visitante',
            'tiros_libres_fallados_local' => 'Tiros Libres Fallados Local',
            'tiros_libres_fallados_visitante' => 'Tiros Libres Fallados Visitante',
            'tiros_3_local' => 'Tiros 3 Local',
            'tiros_3_visitante' => 'Tiros 3 Visitante',
            'tiros_2_local' => 'Tiros 2 Local',
            'tiros_2_visitante' => 'Tiros 2 Visitante',
            'tiro_libre_local' => 'Tiro Libre Local',
            'tiro_libre_visitante' => 'Tiro Libre Visitante',
            'rebotes_local' => 'Rebotes Local',
            'rebotes_visitante' => 'Rebotes Visitante',
            'aisitencias_local' => 'Aisitencias Local',
            'aisitencias_visitante' => 'Aisitencias Visitante',
            'faltas_local' => 'Faltas Local',
            'faltas_visitante' => 'Faltas Visitante',
        ];
    }

    /**
     * Gets query for [[Local]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLocal()
    {
        return $this->hasOne(Equipos::className(), ['id' => 'id_local']);
    }

    /**
     * Gets query for [[Visitante]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVisitante()
    {
        return $this->hasOne(Equipos::className(), ['id' => 'id_visitante']);
    }

    /**
     * Gets query for [[Liga]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLiga()
    {
        return $this->hasOne(Ligas::className(), ['id' => 'id_liga']);
    }
}
