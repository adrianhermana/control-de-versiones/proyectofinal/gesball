<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "telefono".
 *
 * @property int $id
 * @property int $id_jugadores
 * @property string $telefono
 *
 * @property Jugadores $jugadores
 */
class Telefono extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'telefono';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_jugadores', 'telefono'], 'required'],
            [['id_jugadores'], 'integer'],
            [['telefono'], 'string', 'max' => 13],
            [['id_jugadores'], 'exist', 'skipOnError' => true, 'targetClass' => Jugadores::className(), 'targetAttribute' => ['id_jugadores' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_jugadores' => 'Id Jugadores',
            'telefono' => 'Telefono',
        ];
    }

    /**
     * Gets query for [[Jugadores]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJugadores()
    {
        return $this->hasOne(Jugadores::className(), ['id' => 'id_jugadores']);
    }
}
