<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "equipos".
 *
 * @property int $id
 * @property int $id_entrenadores
 * @property int $id_clubs
 * @property int $id_ligas
 * @property string $nombre
 * @property string $licencia_equipo
 *
 * @property Entrenadores $entrenadores
 * @property Clubs $clubs
 * @property Ligas $ligas
 * @property Jugadores[] $jugadores
 * @property Partidos[] $partidos
 * @property Partidos[] $partidos0
 */
class Equipos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'equipos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_entrenadores', 'id_clubs', 'id_ligas', 'nombre', 'licencia_equipo'], 'required'],
            [['id_entrenadores', 'id_clubs', 'id_ligas'], 'integer'],
            [['nombre'], 'string', 'max' => 55],
            [['licencia_equipo'], 'string', 'max' => 4],
            [['id_entrenadores'], 'exist', 'skipOnError' => true, 'targetClass' => Entrenadores::className(), 'targetAttribute' => ['id_entrenadores' => 'id']],
            [['id_clubs'], 'exist', 'skipOnError' => true, 'targetClass' => Clubs::className(), 'targetAttribute' => ['id_clubs' => 'id']],
            [['id_ligas'], 'exist', 'skipOnError' => true, 'targetClass' => Ligas::className(), 'targetAttribute' => ['id_ligas' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_entrenadores' => 'Id Entrenadores',
            'id_clubs' => 'Id Clubs',
            'id_ligas' => 'Id Ligas',
            'nombre' => 'Nombre',
            'licencia_equipo' => 'Licencia Equipo',
        ];
    }

    /**
     * Gets query for [[Entrenadores]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEntrenadores()
    {
        return $this->hasOne(Entrenadores::className(), ['id' => 'id_entrenadores']);
    }

    /**
     * Gets query for [[Clubs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getClubs()
    {
        return $this->hasOne(Clubs::className(), ['id' => 'id_clubs']);
    }

    /**
     * Gets query for [[Ligas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLigas()
    {
        return $this->hasOne(Ligas::className(), ['id' => 'id_ligas']);
    }

    /**
     * Gets query for [[Jugadores]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJugadores()
    {
        return $this->hasMany(Jugadores::className(), ['id_equipos' => 'id']);
    }

    /**
     * Gets query for [[Partidos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPartidos()
    {
        return $this->hasMany(Partidos::className(), ['id_local' => 'id']);
    }

    /**
     * Gets query for [[Partidos0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPartidos0()
    {
        return $this->hasMany(Partidos::className(), ['id_visitante' => 'id']);
    }
}
