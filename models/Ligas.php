<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ligas".
 *
 * @property int $id
 * @property string $nombre
 * @property string $cod_liga
 * @property string $division
 * @property string $temporada
 * @property string $fecha_inicio
 * @property string $fecha_final
 * @property string $num_equipos
 * @property string $comunidad
 * @property string $provincia
 *
 * @property Equipos[] $equipos
 * @property Partidos[] $partidos
 */
class Ligas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ligas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'cod_liga', 'division', 'temporada', 'fecha_inicio', 'fecha_final', 'num_equipos', 'comunidad', 'provincia'], 'required'],
            [['fecha_inicio', 'fecha_final'], 'safe'],
            [['nombre'], 'string', 'max' => 55],
            [['cod_liga'], 'string', 'max' => 4],
            [['division', 'num_equipos'], 'string', 'max' => 2],
            [['temporada'], 'string', 'max' => 9],
            [['comunidad', 'provincia'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'cod_liga' => 'Cod Liga',
            'division' => 'Division',
            'temporada' => 'Temporada',
            'fecha_inicio' => 'Fecha Inicio',
            'fecha_final' => 'Fecha Final',
            'num_equipos' => 'Num Equipos',
            'comunidad' => 'Comunidad',
            'provincia' => 'Provincia',
        ];
    }

    /**
     * Gets query for [[Equipos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEquipos()
    {
        return $this->hasMany(Equipos::className(), ['id_ligas' => 'id']);
    }

    /**
     * Gets query for [[Partidos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPartidos()
    {
        return $this->hasMany(Partidos::className(), ['id_liga' => 'id']);
    }
}
