<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "jugadores".
 *
 * @property int $id
 * @property int $id_equipos
 * @property string $dni
 * @property string $nombre
 * @property string $apellidos
 * @property string $cod_licencia
 * @property string $fecha_licencia
 * @property string $caducidad_licencia
 * @property string $temporada
 * @property string $email
 *
 * @property Equipos $equipos
 * @property Telefono[] $telefonos
 */
class Jugadores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jugadores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_equipos', 'dni', 'nombre', 'apellidos', 'cod_licencia', 'fecha_licencia', 'caducidad_licencia', 'temporada', 'email'], 'required'],
            [['id_equipos'], 'integer'],
            [['fecha_licencia', 'caducidad_licencia'], 'safe'],
            [['dni', 'temporada'], 'string', 'max' => 9],
            [['nombre', 'apellidos'], 'string', 'max' => 55],
            [['cod_licencia'], 'string', 'max' => 4],
            [['email'], 'string', 'max' => 85],
            [['id_equipos'], 'exist', 'skipOnError' => true, 'targetClass' => Equipos::className(), 'targetAttribute' => ['id_equipos' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_equipos' => 'Id Equipos',
            'dni' => 'Dni',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
            'cod_licencia' => 'Cod Licencia',
            'fecha_licencia' => 'Fecha Licencia',
            'caducidad_licencia' => 'Caducidad Licencia',
            'temporada' => 'Temporada',
            'email' => 'Email',
        ];
    }

    /**
     * Gets query for [[Equipos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEquipos()
    {
        return $this->hasOne(Equipos::className(), ['id' => 'id_equipos']);
    }

    /**
     * Gets query for [[Telefonos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTelefonos()
    {
        return $this->hasMany(Telefono::className(), ['id_jugadores' => 'id']);
    }
}
